<?php

namespace Tests\Feature\Issues;

use App\Models\Issue;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class GetWithComplexFilterTest extends TestCase
{

    /** @test */
    public function person_can_get_his_all_active_issues()
    {
        $me = createPerson();
        $category = createCategory();

        /** @var Issue $my_issue1 */
        $my_issue1 = genIssue(['state' => Issue::STATE_NEW], [], $me)[0];
        /** @var Issue $my_issue2 */
        $my_issue2 = genIssue(['state' => Issue::STATE_ACTIVE], [], $me)[0];


        $him = createPerson();

        /** @var Issue $his_issue1 */
        $his_issue1 = genIssue([], $category, $him)[0];
        /** @var Issue $his_issue2 */
        $his_issue2 = genIssue([], $category, $him)[0];


        /** Person want to see his all active issues */
        $filter = [
            'person_id' => $me->id,
            'state'     => Issue::STATE_ACTIVE,
        ];

        $this
            ->getJson(route('issues.index', $filter))
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonCount(1, 'data')
            ->assertJson([
                'data' => [
                    ['id' => $my_issue2->id]
                ]
            ]);


        /** Person want to see his all issues under moderation */
        $filter = [
            'person_id' => $me->id,
            'state'     => Issue::STATE_NEW,
        ];

        $this
            ->getJson(route('issues.index', $filter))
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonCount(1, 'data')
            ->assertJson([
                'data' => [
                    ['id' => $my_issue1->id]
                ]
            ]);


        /** Person want to see his all issues in category */
        $filter = [
            'person_id'   => $him->id,
            'category_id' => $category->id,
        ];

        $this->get(route('issues.index', $filter))
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonCount(2, 'data');
    }
}
