<?php

namespace Tests\Feature\Issues;

use App\Models\Category;
use App\Models\Document;
use App\Models\Issue;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class GetIssuesWithRelationshipsTest extends TestCase
{

    /** @test */
    public function person_can_get_issues_with_category()
    {
        /** @var Issue $issue */
        /** @var Category $category */
        [$issue, $category] = genIssue();


        $this
            ->getJson(route('issues.index', ['withCategories' => true]))
            ->assertStatus(Response::HTTP_OK)
            ->assertJson([
                'data' => [
                    ['id' => $issue->id, 'category' => ['id' => $category->id]]
                ]
            ]);
    }

    /** @test */
    public function person_can_get_issues_with_documents()
    {
        /** @var Issue $issue */
        [$issue] = genIssue();

        $document = $issue->attachDocument(makeUploadedFile());

        $this
            ->getJson(route('issues.index', ['withDocuments' => true]))
            ->assertJson([
                'data' => [
                    ['id' => $issue->id, 'documents' => [['id' => $document->id]]]
                ]
            ]);
    }
}
