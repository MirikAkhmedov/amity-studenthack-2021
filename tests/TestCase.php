<?php

namespace Tests;

use App\Models\Document;
use File;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();

        $this->withoutExceptionHandling();
    }

    protected function tearDown(): void
    {
        foreach (Document::all() as $document) File::delete($document->full_path);

        parent::tearDown();
    }
}
