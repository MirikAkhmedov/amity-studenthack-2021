<?php

use App\Models\Category;
use App\Models\Issue;
use App\Models\Person;

/**
 *
 * Generates fake issue
 *
 * @param array $attributes - Attributes of Issue
 * @param array|Category $category_attributes - Attributes of Category, Pass Category model to create in specified category
 * @param array|Person $person_attributes - Attributes of Person, Pass Person model to create for specified person
 *
 * @return array
 *
 */
function genIssue($attributes = [], $category_attributes = [], $person_attributes = []): array
{
    $category = $category_attributes instanceof Category
        ? $category_attributes
        : createCategory($category_attributes);


    $person = $person_attributes instanceof Person
        ? $person_attributes
        : createPerson($person_attributes);

    $attributes['category_id'] = $category->id;
    $attributes['person_id'] = $person->id;

    $issue = Issue::factory()->create($attributes);

    return [$issue, $category, $person];
}
