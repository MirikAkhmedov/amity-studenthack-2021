<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class RealDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = collect([
            "Gaz",
            "Elektr energiyasi",
            "Ichimlik suvi",
            "Issiv suv va isitish tizimi",
            "Chiqindilar",
            "Atrof muhit muhofazasi",
            "Yo'l qurilish",
            "Qurilish vazirligi",
            "Aloqa va telefoniya",
            "IIB",
            "FVV",
            "Turizm agentligi"
        ]);

        $categories->each(function ($category) {
            Category::create([
                'name' => $category
            ]);
        });
    }
}
