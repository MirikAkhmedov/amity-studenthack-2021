<?php

namespace Database\Factories;

use App\Models\Person;
use Illuminate\Database\Eloquent\Factories\Factory;

class PersonFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Person::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'        => $this->faker->firstName,
            'telegram_id' => $this->faker->numberBetween('1000000', '999999999'),
            'phone'       => $this->faker->e164PhoneNumber,
        ];
    }
}
