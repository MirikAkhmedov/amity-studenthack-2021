import Vue from 'vue'
import Vuex from 'vuex'
import axios from "./axios";

Vue.use(Vuex)

export let centerPos = {lat: 41.311081, lng: 69.240562}

const store = new Vuex.Store({
    state: {
        currentPos: centerPos,
        categories: [],
        selectedCategories: [],

        issueStates: {
            0: 'Yangi',
            1: `Ko'rib chiqilmoqda`,
            2: `Hal qilindi`,
            3: `Rad etildi`,
        },

        isLoggedIn: undefined
    },

    getters: {
        getCurrentPos({currentPos: {lat, lng}}) {
            return `${lat.toFixed(6)}, ${lng.toFixed(6)}`
        },

        isLoggedIn(state) {
            return state.isLoggedIn
        }
    },

    mutations: {
        setLoggedIn(state, status) {
            state.isLoggedIn = status
        }
    },

    actions: {
        async checkLogin({commit}) {
            let auth = false;

            try {
                let res = await axios.get('/user/status')
                auth = res.status === 200;
            } catch {

            }

            commit('setLoggedIn', auth)

            return auth;
        }
    }
})

export default store;
