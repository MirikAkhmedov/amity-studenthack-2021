<?php

function reverse_geocode($lat, $lon)
{
    return Http::get('https://eu1.locationiq.com/v1/reverse.php', [
        'key'    => 'pk.9de69b1e17ecae7b9a8fff712c21e857',
        'lat'    => $lat,
        'lon'    => $lon,
        'format' => 'json',
    ])->json();
}
