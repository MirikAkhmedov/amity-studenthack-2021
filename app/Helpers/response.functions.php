<?php

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;

function responseJson($data, string $message = '', int $status = 200): JsonResponse
{
    return Response::json(compact('data', 'message'), $status);
}

function responseMessage(string $message, int $status = 200): JsonResponse
{
    return Response::json(compact('message'), $status);
}

function responseData($data, int $status = 200): JsonResponse
{
    return Response::json(compact('data'), $status);
}

function responseJsonFile($data, $filename = 'export.json')
{
    return Response::make($data, 200, [
        'Content-type'        => 'text/json',
        'Content-Disposition' => "attachment; filename=$filename",
    ]);
}
