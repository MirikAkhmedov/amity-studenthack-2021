<?php

namespace App\Admin\Controllers;

use App\Models\Issue;
use App\Models\Person;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Encore\Admin\Widgets\Table;

class PeopleController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Fuqarolar';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Person());

        $grid->column('id', 'ID');
        $grid->column('name', 'Ismi')
            ->modal('Shikoyatlari', function (Person $model) {
                $issues = $model
                    ->load('issues.category')
                    ->issues
                    ->map(function (Issue $issue) {
                        return [
                            'id'       => $issue->id,
                            'category' => $issue->category->name,
                            'state'    => $issue->state_text,
                        ];
                    });

                return new Table(['id' => 'ID', 'category' => 'Tashkilotlar', 'state' => 'Holati'], $issues->toArray());

            });
        $grid->column('telegram_id', 'Telegram ID');
        $grid->column('phone', 'Telefon raqami');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Person::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name', __('Name'));
        $show->field('telegram_id', __('Telegram id'));
        $show->field('phone', __('Phone'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Person());

        $form->text('name', __('Name'));
        $form->text('telegram_id', __('Telegram id'));

        return $form;
    }
}
