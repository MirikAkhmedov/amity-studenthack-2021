<?php

namespace App\Admin\Controllers;

use App\Models\Category;
use App\Models\Issue;
use App\Models\Person;
use Carbon\Carbon;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class IssuesController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Shikoyatlar';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Issue());

        $grid->model()->orderByDesc('id');

        $grid->column('id', 'ID');
        $grid->column('category.name', 'Tashkilot');
        $grid->column('person.name', 'Fuqaro');
        $grid->column('title', __('Title'));
        $grid->column('region', 'Viloyat');
        $grid->column('district', 'Tuman');
        $grid->column('delivered', 'Yetkazildi')->bool();
        $grid->column('state', __('Shikoyat holati'))
            ->editable('select', Issue::getStates());
        $grid->column('created_at', 'Shikoyat vaqti')
            ->display(fn($time) => Carbon::parse($time)->format('d M, Y'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Issue::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('category_id', __('Category id'));
        $show->field('person_id', __('Person id'));
        $show->field('state', __('State'));
        $show->field('title', __('Title'));
        $show->field('description', __('Description'));
        $show->field('location', __('Location'));
        $show->field('long', __('Long'));
        $show->field('lat', __('Lat'));
        $show->field('region', __('Region'));
        $show->field('district', __('District'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Issue());

        $form->select('category_id', 'Tashkilot')
            ->options(Category::all()->pluck('name', 'id'));
        $form->select('person_id', 'Fuqaro')
            ->options(Person::all()->pluck('name', 'id'));
        $form->select('state', 'Holati')->options(Issue::getStates());
        $form->text('title', __('Title'));
        $form->textarea('description', __('Description'));
        $form->textarea('location', __('Location'));
        $form->text('long', __('Long'));
        $form->text('lat', __('Lat'));
        $form->text('region', __('Region'));
        $form->text('district', __('District'));

        return $form;
    }
}
