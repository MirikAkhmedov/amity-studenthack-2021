<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Issue */
class IssueResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'               => $this->id,
            'state'            => $this->state,
            'title'            => $this->title,
            'description'      => $this->description,
            'location'         => $this->location,
            'lat'              => $this->lat,
            'long'             => $this->long,
            'region'           => $this->region,
            'district'         => $this->district,
            'created_at'       => $this->created_at,
            'updated_at'       => $this->updated_at,

            'category_id'      => $this->category_id,
            'person_id'        => $this->person_id,

            'votes'            => $this->votes->count(),
            'has_voted'        => $this->has_voted,

            'response_message' => $this->response_message,

            'category'         => new CategoryResource($this->whenLoaded('category')),
            'person'           => new PersonResource($this->whenLoaded('person')),
            'documents'        => DocumentResource::collection($this->whenLoaded('documents')),
        ];
    }
}
