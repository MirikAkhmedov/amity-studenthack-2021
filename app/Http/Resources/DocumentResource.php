<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Document */
class DocumentResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'type'       => $this->type,
            'name'       => $this->name,
            'url'        => url('storage/' . $this->path),
            'created_at' => $this->created_at,
            'kind'       => $this->kind,

            'issue' => new IssueResource($this->whenLoaded('issue')),
        ];
    }
}
