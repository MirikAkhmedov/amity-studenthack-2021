<?php

namespace App\Http\Controllers;

use App\Models\Issue;
use Auth;

class VoteController extends Controller
{
    public function vote()
    {
        $issue = Issue::find(request('issue_id'));

        $issue->people()->toggle([Auth::guard('person')->id()]);

        return responseData($issue->votes->count());
    }
}
