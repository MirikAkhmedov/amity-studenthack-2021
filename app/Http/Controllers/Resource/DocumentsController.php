<?php

namespace App\Http\Controllers\Resource;

use App\Http\Controllers\Controller;
use App\Http\Resources\DocumentResource;
use App\Models\Issue;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class DocumentsController extends Controller
{
    public function store(Request $request): JsonResponse
    {
        $issue = Issue::findOrFail($request['issue_id']);
        $document = $issue->attachDocument($request['file'], $request['kind']);

        return responseJson(new DocumentResource($document), trans('messages.document_created'), Response::HTTP_CREATED);
    }
}
