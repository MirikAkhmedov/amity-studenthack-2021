<?php

namespace App\Http\Controllers\Resource;

use App\Http\Controllers\Controller;
use App\Http\Resources\IssueResource;
use App\Jobs\DeliverIssue;
use App\Models\Issue;
use Arr;
use Auth;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class IssuesController extends Controller
{
    public function index(Request $request)
    {
        $issues = Issue::query();

        if ($request->has('category_id')) {
            $issues->where('category_id', $request->get('category_id'));
        }

        if ($request->has('person_id')) {
            $issues->where('person_id', $request->get('person_id'));
        }

        if ($request->has('state')) {
            $issues->where('state', $request->get('state'));
        }

        if ($request->has('withPeople')) {
            $issues->with(['person']);
        }

        if ($request->has('withCategories')) {
            $issues->with(['category']);
        }

        if ($request->has('withDocuments')) {
            $issues->with(['documents']);
        }

        $issues = IssueResource::collection($issues->get());

        return $request->has('export')
            ? responseJsonFile($issues)
            : responseData($issues);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data['person_id'] = Auth::guard('person')->id();

        $geocode = reverse_geocode($data['lat'], $data['long']);

        $data['location'] = Arr::get($geocode, 'display_name');
        $data['region'] = Arr::get($geocode, 'address.state') ?? Arr::get($geocode, 'address.city');
        $data['district'] = Arr::get($geocode, 'address.country');

        $issue = Issue::create($data);

        dispatch(new DeliverIssue($issue));

        $issue->people()->syncWithoutDetaching($data['person_id']);

        return responseJson(
            new IssueResource($issue),
            trans('messages.issue_created'),
            Response::HTTP_CREATED
        );
    }
}
