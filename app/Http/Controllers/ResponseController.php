<?php

namespace App\Http\Controllers;

use App\Models\Issue;

class ResponseController extends Controller
{
    public function response()
    {
        $issue = Issue::find(request('issue_id'));

        $issue->update(
            request()->validate([
                'response_message' => '',
                'state'            => '',
            ])
        );

        return responseMessage(trans('admin.update_succeeded'));
    }
}
