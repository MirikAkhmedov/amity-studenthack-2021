<?php

namespace App\Jobs;

use App\Http\Resources\IssueResource;
use App\Models\Issue;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use URL;

class DeliverIssue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Issue
     */
    private Issue $issue;

    public $tries = 5;

    /**
     * Create a new job instance.
     *
     * @param Issue $issue
     */
    public function __construct(Issue $issue)
    {
        //
        $this->issue = $issue;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->issue->load(['category', 'person', 'documents']);
        $url = $this->issue->category->webhook_url;

        if (!URL::isValidUrl($url)) {
            $this->issue->update([
                'delivered' => -1
            ]);
        } else {
            $data = (new IssueResource($this->issue))->jsonSerialize();

            $response = Http::post($url, $data);

            if ($response->successful()) {
                $this->issue->update([
                    'delivered' => 1
                ]);
            } else {
                throw new \Error("Cannot deliver");
            }
        }
    }
}
