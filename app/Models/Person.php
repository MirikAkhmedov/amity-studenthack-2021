<?php

namespace App\Models;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Person extends Model implements Authenticatable
{
    use HasFactory;

    protected static $unguarded = true;

    public static function createNew($data)
    {
        return Person::create([
            'telegram_id' => $data['id'],
            'name' => $data['first_name'] . " " . $data["last_name"],
            'tg_username' => $data['username'],
            'phone' => ''
        ]);
    }

    public function issues()
    {
        return $this->hasMany(Issue::class);
    }

    public function getAuthIdentifierName()
    {
        return 'id';
    }

    public function getAuthIdentifier()
    {
        return $this->{$this->getAuthIdentifierName()};
    }

    public function getAuthPassword()
    {
        return $this->password;
    }

    public function getRememberToken()
    {
        return $this->{$this->getRememberTokenName()};
    }

    public function setRememberToken($value)
    {
        $this->setAttribute($this->getRememberTokenName(), $value);
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }
}
