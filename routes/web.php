<?php

use App\Http\Controllers\MainController;
use App\Http\Controllers\ResponseController;
use App\Http\Controllers\TelegramAuthController;
use App\Http\Controllers\VoteController;
use App\Models\Person;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Resource\CategoriesController;
use App\Http\Controllers\Resource\DocumentsController;
use App\Http\Controllers\Resource\IssuesController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if (Auth::guest()) {
    $person = Person::inRandomOrder()->first();

    Auth::guard('person')->login($person);
}

Route::redirect('/', '/map');

Route::get('/map', [MainController::class, 'map'])->name('map');
Route::get('/telegram-login', [TelegramAuthController::class, 'handleTelegramCallback']);

Route::get('/user/status', [TelegramAuthController::class, 'isLoggedIn'])->middleware('auth:person');

Route::apiResource('categories', CategoriesController::class)
    ->only('index');

Route::apiResource('issues', IssuesController::class)
    ->only('store', 'index');

Route::apiResource('documents', DocumentsController::class)
    ->only('store');

Route::get('reverse-geocode/{lat}/{long}', fn($lat, $long) => reverse_geocode($lat, $long))
    ->name('reverse-geocode');

Route::post('vote', [VoteController::class, 'vote']);
Route::post('response', [ResponseController::class, 'response']);
